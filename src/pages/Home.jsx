import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { ItemBlock } from '../Components';
import { addDishesToCart } from '../redux/actions/cart'

function Home() {
    const dispatch = useDispatch();
    const items = useSelector(({dishes}) => dishes.items);
    const cartItems = useSelector(({cart}) => cart.items);

    const handleAddDishesToCart = (obj) => {
      dispatch({
          type: 'ADD_DISHES_CART',
          payload: obj,
      })
    };

    return (
        <div className="menu-container">
            <div className="menu-list">
                {items && items.map((obj) => (
                    <ItemBlock
                        onClickAddDishes={handleAddDishesToCart}
                        key={obj.id}
                        addedCount={cartItems[obj.id] && cartItems[obj.id].items.length}
                        {...obj} />
                ))}
            </div>
        </div>
    )
}
export default Home;
