import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { CartItem } from '../Components';
import { removeCartItem, incrementItem, decrementItem } from '../redux/actions/cart'
import {ButtonPay} from "../Components/index";


function Cart() {
    const dispatch = useDispatch();
    const { totalPrice, items } = useSelector(({ cart }) => cart);

    const addedDishes = Object.keys(items).map((key) => {
        return items[key].items[0];
    });

    const onRemoveItem = (id) => {
      if (window.confirm('Вы действительно хотите удалить?')) {
          dispatch(removeCartItem(id));
      }
    };

    const onIncrementItem = (id) => {
        dispatch(incrementItem(id));
    }
    const onDecrementItem = (id) => {
        dispatch(decrementItem(id));
    }

    const onClickOrder = () => {
        console.log('ВАШ ЗАКАЗ', items);
    }
    
    return (
        <div className="cart-page">
            <header className="header-cart">
                <h1 className="header__title-cart">Корзина с выбранными товарами</h1>
            </header>

            <div className="cart-list">
                {addedDishes.map((obj) => (
                    <CartItem
                        key={obj.id}
                        id={obj.id}
                        name={obj.name}
                        image={obj.image}
                        totalPrice={items[obj.id].totalPrice}
                        totalCount={items[obj.id].items.length}
                        onRemove={onRemoveItem}
                        onMinus={onDecrementItem}
                        onPlus={onIncrementItem}
                    />
                ))}
            </div>
            <div className="cart-foot">
                <div className="cart-foot__block">
                    <div className="cart-foot__order-sum">
                        <span className="cart-foot__order">Заказ на сумму:</span>
                        <span className="cart-foot__sum">{totalPrice} ₽</span>
                    </div>
                    <ButtonPay onClick={onClickOrder} className="cart-foot__btn">Оформить заказ</ButtonPay>
                </div>
            </div>
        </div>
    )
}

export default Cart;