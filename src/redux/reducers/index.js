import { combineReducers } from 'redux';

import dishes from './dishes';
import cart from './cart';

const rootReducer = combineReducers({
    dishes,
    cart,
});

export default rootReducer;
