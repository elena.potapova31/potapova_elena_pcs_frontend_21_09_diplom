export const setDishes = (items) => ({
    type: 'SET_DISHES',
    payload: items,
});