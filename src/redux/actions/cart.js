export const addDishesToCart = (dishesObj) => ({
    type: 'ADD_DISHES_CART',
    payload: dishesObj,
});

export const removeCartItem = (id) => ({
    type: 'REMOVE_CART_ITEM',
    payload: id,
});

export const incrementItem = (id) => ({
    type: 'INCREMENT_CART_ITEM',
    payload: id,
});

export const decrementItem = (id) => ({
    type: 'DECREMENT_CART_ITEM',
    payload: id,
});