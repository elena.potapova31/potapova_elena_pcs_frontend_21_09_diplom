import React from 'react';
import classNames from 'classnames';

const ButtonDeleteDishes = ({ onClick, children }) => {
    return (
        <button
            onClick={onClick}
            className={classNames('cart-item__btn')
            }>
            {children}
        </button>
    );
};

export default ButtonDeleteDishes