import React from 'react';
import {ButtonDeleteDishes} from './index';
import {ButtonPlusMinus} from "./index";

function CartItem ({ id, name, image, totalPrice, totalCount, onRemove, onMinus, onPlus  }) {
   const handleRemoveClick = () => {
       onRemove(id);
   }
   const handlePlusItems = () => {
       onPlus(id);
   }
    const handleMinusItems = () => {
        onMinus(id);
    }

    return (
        <div className="cart-item">
            <img className="cart-item__img" src={image} alt="Устрицы по рокфеллеровски"/>
            <div className="cart-item__title">{name}</div>
            <div className="cart__btn-inc-dec">
                <ButtonPlusMinus onClick={handleMinusItems} className="cart__btn-inc">-</ButtonPlusMinus>
                <div className="cart__btn-cnt">{totalCount}</div>
                <ButtonPlusMinus onClick={handlePlusItems} className="cart__btn-inc">+</ButtonPlusMinus>
            </div>

            <div className="cart-item__price-btn">
                <div className="cart-item__price">{totalPrice} ₽</div>
                <ButtonDeleteDishes onClick={handleRemoveClick} className="cart-item__btn">x</ButtonDeleteDishes>
            </div>
        </div>
    )
}
export default CartItem;