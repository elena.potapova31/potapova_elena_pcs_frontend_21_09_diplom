import React from 'react';
import { useSelector } from "react-redux";
import {ButtonCart} from './index';
import {Link, Route, Routes} from 'react-router-dom';


function HeaderMenuList() {
    const { totalPrice, totalCount } = useSelector(({ cart }) => ({
        totalPrice: cart.totalPrice,
        totalCount: cart.totalCount,
    }));
    return (
        <header>
            <Link to="/">
                <h1 className="header__title">наша продукция</h1>
                <Routes>
                    <Route exact path="/" />
                </Routes>
            </Link>
                <div className="header__nav-cart">
                    <span className="header__nav-cart-count">товаров ({totalCount}) на сумму ({totalPrice})₽</span>
                    <Link to="/Cart">
                        <ButtonCart className="header__nav-cart-icon">
                            <img className="header__nav-cart-icon-img" src="/image/Vector.png" alt="cart" id="button-cart_img" width="21" height="21"/>
                        </ButtonCart>
                    </Link>
                </div>
        </header>
    )
}
export default HeaderMenuList;