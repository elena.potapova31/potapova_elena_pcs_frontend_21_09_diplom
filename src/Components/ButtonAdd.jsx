import React from 'react';
import classNames from 'classnames';

const ButtonAdd = ({ onClick, children }) => {
    return (
        <button
            onClick={onClick}
            className={classNames('button-add')
            }>
            {children}
        </button>
    );
};

export default ButtonAdd