import React from 'react';
import classNames from 'classnames';

const ButtonPlusMinus = ({ onClick, children }) => {
    return (
        <button
            onClick={onClick}
            className={classNames('cart__btn-inc')
            }>
            {children}
        </button>
    );
};

export default ButtonPlusMinus