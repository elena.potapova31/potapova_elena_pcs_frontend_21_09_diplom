import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types'

const ButtonCart = ({ onClick, children }) => {
    return (
        <button
            onClick={onClick}
            className={classNames('header__nav-cart-icon')
            }>
            {children}
        </button>
    );
};
ButtonCart.propTypes = {
    onClick: PropTypes.func,
}

export default ButtonCart
