import React from 'react';
import PropTypes from 'prop-types'
import { ButtonAdd } from './index'

function ItemBlock({ id, name, image, description, price, weight, onClickAddDishes, addedCount }) {
    const onAddDishes = () => {
        const obj = {
            id,
            name,
            image,
            price,
        }
        onClickAddDishes(obj);
    };

    return (
        <div className="menu-item">
            <img className="menu-item__img" src={image} alt="Устрицы по рокфеллеровски" />
            <div className="menu-item__info">
                <div className="menu-item__title">{name}</div>
                <span className="menu-item__description">
                    {description}
                </span>
                <div className="menu-item__row">
                    <div className="menu-item__price-weight-wrapper">
                        <div className="menu-item__price">{price} ₽</div>
                        <div className="menu-item__weight">/ {weight} </div>
                    </div>
                    <ButtonAdd onClick={onAddDishes} className="button-add">
                        <div className="menu-item__cart">
                            <div className="menu-item__btn">+</div>
                            <div className="menu-item__amt">{addedCount}</div>
                        </div>
                    </ButtonAdd>
                </div>
            </div>
        </div>
    )
}

ItemBlock.propTypes = {
    name: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    weight: PropTypes.string.isRequired,
    addedCount: PropTypes.number.isRequired,
};
ItemBlock.defaultProps = {
    name: '---',
    image: '---',
    description: '---',
    price: 0,
    weight: '0 г.',
}
export default ItemBlock;