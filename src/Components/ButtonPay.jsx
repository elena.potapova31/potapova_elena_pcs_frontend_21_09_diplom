import React from 'react';
import classNames from 'classnames';

const ButtonPay = ({ onClick, children }) => {
    return (
        <button
            onClick={onClick}
            className={classNames('cart-foot__btn')
            }>
            {children}
        </button>
    );
};

export default ButtonPay