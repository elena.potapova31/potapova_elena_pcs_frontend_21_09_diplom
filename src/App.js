import React  from 'react';
import axios from 'axios'
import { useDispatch } from 'react-redux';

import { HeaderMenuList } from './Components';
import { Home, Cart } from './pages'
import { Routes, Route, Outlet } from "react-router-dom";
import { setDishes } from "./redux/actions/dishes";

function App() {
    const dispatch = useDispatch();

    React.useEffect(() => {
        axios.get('http://localhost:3000/db.json').then(({ data }) => {
            dispatch(setDishes(data.dishes));
        });
    }, []);

    return (
        <Routes>
            <Route path="/" element={<Layout/>} >
                <Route index element={<Home />} />
                <Route path="Cart" element={<Cart/>} />
            </Route>
        </Routes>
    )
}

function Layout() {
    return (
        <div className="page">
            <HeaderMenuList />
            <div className="content">
                <Outlet />
            </div>
        </div>
    )
}

export default App;

// export default connect(
//     (state) => {
//         return {
//             items: state.dishes.items,
//         };
//     },
//     (dispatch) => {
//         return {
//             setDishes: (items) => dispatch(setDishes(items)),
//         };
//     },
// ) (App);